<?php
/** For more info about namespaces plase @see http://php.net/manual/en/language.namespaces.importing.php */
namespace Mini\Core;

class Application
{
    /** @var null The controller */
    private $urlController = null;

    /** @var null The method (of the above controller), often also named "action" */
    private $urlAction = null;

    /** @var array URL parameters */
    private $urlParams = array();

    /**
     * Function in controller with this prefix will be access on the web,
     * by controller name and function name
     * Eg: HomeController, function: actionIndex => access /home/index
     */
    private $routerPrefix = 'action';

    /**
     * "Start" the application:
     * Analyze the URL elements and calls the according controller/method or the fallback
     */
    public function __construct()
    {
        // create array with URL parts in $url
        $this->splitUrl();

        // check for controller: no controller given ? then load start-page
        if (!$this->urlController) {

            $page = new \Mini\Controller\HomeController();
            $page->actionIndex();

        } elseif (file_exists(APP . 'Controller/' . ucfirst($this->urlController) . 'Controller.php')) {
            // here we did check for controller: does such a controller exist ?

            // if so, then load this file and create this controller
            // like \Mini\Controller\CarController
            $controller = "\\Mini\\Controller\\" . ucfirst($this->urlController) . 'Controller';
            $this->urlController = new $controller();

            // check for method: does such a method exist in the controller ?
            if (method_exists($this->urlController, $this->urlAction)) {

                if (!empty($this->urlParams)) {
                    // Call the method and pass arguments to it
                    call_user_func_array(array($this->urlController, $this->urlAction), $this->urlParams);
                } else {
                    // If no parameters are given, just call the method without parameters, like $this->home->method();
                    $this->urlController->{$this->urlAction}();
                }

            } else {
                if (strlen($this->urlAction) == 0 && method_exists($this->urlController, 'actionIndex')) {
                    // no action defined: call the default index() method of a selected controller
                    $this->urlController->actionIndex();
                } else {
                    header('location: ' . URL . 'error');
                }
            }
        } else {
            header('location: ' . URL . 'error');
        }
    }

    /**
     * Get and split the URL
     */
    private function splitUrl()
    {
        if (isset($_GET['url'])) {

            // split URL
            $url = trim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            // Put URL parts into according properties
            // By the way, the syntax here is just a short form of if/else, called "Ternary Operators"
            // @see http://davidwalsh.name/php-shorthand-if-else-ternary-operators
            $this->urlController = isset($url[0]) ? $url[0] : null;
            $this->urlAction = isset($url[1]) ? $this->routerPrefix . $url[1] : null;

            // Remove controller and action from the split URL
            unset($url[0], $url[1]);

            // Rebase array keys and store the URL params
            $this->urlParams = array_values($url);

            // for debugging. uncomment this if you have problems with the URL
            //echo 'Controller: ' . $this->urlController . '<br>';
            //echo 'Action: ' . $this->urlAction . '<br>';
            //echo 'Parameters: ' . print_r($this->urlParams, true) . '<br>';
        }
    }
}
