# PHP Project Skeleton
Based on [MINI3](https://github.com/panique/mini3), thanks to [panique](https://github.com/panique), [JaoNoctus](https://github.com/JaoNoctus)

## Requirements

- PHP 5.6 or PHP 7.0
- MySQL
- mod_rewrite activated (see below for tutorials)
- Composer

## Installation (Windows)

1. Edit the database credentials in `application/config/config.php`
2. Execute the .sql statements in the `_install/`-folder (with PHPMyAdmin for example).
3. Install web server [XAMPP](https://www.apachefriends.org/xampp-files/7.0.9/xampp-win32-7.0.9-1-VC14-installer.exe)
4. Install [Composer](http://getcomposer.org)
5. Run `composer install` in the project's folder to create the PSR-4 autoloading stuff from Composer automatically.
   If you have no idea what this means: Remember the "good" old times when we were using "include file.php" all over our projects to include and use something ?
   PSR-0/4 is the modern, clean and automatic version of that. Please have a google research if that's important for you. 
6. If something is not working, please make sure you have mod_rewrite activated on your server / in your environment. Some guidelines:
   [XAMPP for Windows](http://www.leonardaustin.com/blog/technical/enable-mod_rewrite-in-xampp/)

## Features

- Extremely simple, easy to understand
- Simple but clean structure
- Makes "beautiful" clean URLs
- Demo CRUD actions: Create, Read, Update and Delete database entries easily, demo AJAX call
- Tries to follow PSR coding guidelines
- Uses PDO for any database requests, comes with an additional PDO debug tool to emulate your SQL statements
- Uses only native PHP code, so people don't have to learn a framework
- Uses PSR-4 autoloader

## Security

The script makes use of mod_rewrite and blocks all access to everything outside the /public folder.
Your .git folder/files, operating system temp files, the application-folder and everything else is not accessible
(when set up correctly). For database requests PDO is used, so no need to think about SQL injection (unless you
are using extremely outdated MySQL versions).

## Quick-Start

#### How to include stuff / use PSR-4

As this project uses proper PSR-4 namespaces, make sure you load/use your stuff correctly:
Instead of including classes with old-school code like `include xxx.php`, simply do something like `use Mini\Model\Song;` on top of your file (modern IDEs even do that automatically).
This would automatically include the file *Song.php* from the folder *Mini/Model* (it's case-sensitive!).
 
But wait, there's no `Mini/Model/Song.php` in the project, but a `application/Model/Song.php`, right ?
To keep things cleaner, the composer.json sets a *namespace* (see code below), which is basically a name or an alias, for a certain folder / area of your application,
in this case the folder `application` is now reachable via `Mini` when including stuff.

```
{
    "psr-4":
    {
        "Mini\\" : "application/"
    }
}
```

This might look stupid at first, but comes in handy later. To sum it up:

To load the file `application/Model/Song.php`, write a `use Mini\Model\Song;` on top of your controller file.
Have a look into the SongController to get an idea how everything works!

FYI: As decribed in the install tutorial, you'll need do perform a "composer install" when setting up your application for the first time, which will
create a set of files (= the autoloader) inside /vendor folder. This is the normal way Composer handle this stuff. If you delete your vendor folder 
the autoloading will not work anymore. If you change something in the composer.json, always make sure to run composer install/update again!

#### The structure in general

The application's URL-path translates directly to the controllers (=files) and their methods inside
application/controllers.

`example.com/home/exampleOne` will do what the *actionExampleOne()* method in `application/Controller/HomeController.php` says.

`example.com/home` will do what the *actionIndex()* method in `application/Controller/HomeController.php` says.

`example.com` will do what the *actionIndex()* method in `application/Controller/HomeController.php` says (default fallback).

`example.com/songs` will do what the *actionIndex()* method in `application/Controller/SongsController.php` says.

`example.com/songs/editsong/17` will do what the *actionEditsong()* method in `application/Controller/SongsController.php` says and will pass `17` as a parameter to it.

Self-explaining, right ?

#### Showing a view

Let's look at the exampleOne()-method in the home-controller (application/Controller/HomeController.php): This simply shows the header, footer and the example_one.php page (in views/home/). By intention as simple and native as possible.

```php
public function exampleOne()
{
    // load view
    require APP . 'views/_templates/header.php';
    require APP . 'views/home/example_one.php';
    require APP . 'views/_templates/footer.php';
}
```  

#### Working with data

Let's look into the index()-method in the songs-controller (application/Controller/SongsController.php): Similar to exampleOne,
but here we also request data. Again, everything is extremely reduced and simple: $Song->getAllSongs() simply
calls the getAllSongs()-method in application/Model/Song.php (when $Song = new Song()).

```php
namespace Mini\Controller

use Mini\Model\Song;

class SongsController
{
    public function index()
    {
        // Instance new Model (Song)
        $Song = new Song();
        // getting all songs and amount of songs
        $songs = $Song->getAllSongs();
        $amount_of_songs = $Song->getAmountOfSongs();

        // load view. within the view files we can echo out $songs and $amount_of_songs easily
        require APP . 'views/_templates/header.php';
        require APP . 'views/songs/index.php';
        require APP . 'views/_templates/footer.php';
    }
}
```

For extreme simplicity, data-handling methods are in application/model/ClassName.php. Have a look how getAllSongs() in model.php looks like: Pure and
super-simple PDO.

```php
namespace Mini\Model

use Mini\Core\Model;

class Song extends Model
{
    public function getAllSongs()
    {
        $sql = "SELECT id, artist, track, link FROM song";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
}
```

The result, here $songs, can then easily be used directly
inside the view files (in this case application/views/songs/index.php, in a simplified example):

```php
<tbody>
<?php foreach ($songs as $song) { ?>
    <tr>
        <td><?php if (isset($song->artist)) echo htmlspecialchars($song->artist, ENT_QUOTES, 'UTF-8'); ?></td>
        <td><?php if (isset($song->track)) echo htmlspecialchars($song->track, ENT_QUOTES, 'UTF-8'); ?></td>
    </tr>
<?php } ?>
</tbody>
```

## Goodies

MINI3 comes with a little customized [PDO debugger tool](https://github.com/panique/pdo-debug) (find the code in
application/libs/helper.php), trying to emulate your PDO-SQL statements. It's extremely easy to use:

```php
$sql = "SELECT id, artist, track, link FROM song WHERE id = :song_id LIMIT 1";
$query = $this->db->prepare($sql);
$parameters = array(':song_id' => $song_id);

echo Helper::debugPDO($sql, $parameters);

$query->execute($parameters);
```

## License

This project is licensed under the MIT License.
This means you can use and modify it for free in private or commercial projects.
